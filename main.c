#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

/**
 * Function Prototype
 */
pthread_t create_thread(void *(*start_routine) (void *), void *arg);
pid_t create_process(void (*routine)());

/**
 * Info Structure
 * 스레드에 정보를 보내기위해 사용되는 자료구조
 */
typedef struct _Info {
	char process[256];
	char name[256];
	time_t delay;
} Info;

/**
 * Busy Waiting
 * @param sec
 * @param nsec
 */
void delay(time_t sec, long nsec)
{
	struct timespec tim, tim2;
	tim.tv_sec = sec;
	tim.tv_nsec = nsec;

	nanosleep(&tim, &tim2);
}

/**
 * 스레드를 생성한다
 * @param start_routine
 * @return
 */
pthread_t create_thread(void *(*start_routine) (void *), void *arg)
{
	pthread_t thread_id;
	pthread_attr_t attr;

	if( pthread_attr_init(&attr) != 0 ) {
		return 0;
	}

	if( pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) != 0 ) {
		return 0;
	}

	if (pthread_create(&thread_id, NULL, start_routine, arg) < 0 ) {
		return 0;
	}

	if( pthread_attr_destroy(&attr) != 0 ) {
		return 0;
	}

	return thread_id;
}

/**
 * 프로세스를 생성한다
 * 인자를 통해서 자식 프로세스에서 실행할 루틴을 보낼 수 있다
 * @param routine
 * @return
 */
pid_t create_process(void (*routine)())
{
	pid_t pid = fork();

	switch (pid) {
		case -1:
			fprintf(stderr, "Creating Process Failed");
			break;
		case 0:
			routine();
			exit(0);
		default:
			return pid;
	}

	return -1;
}

void *thread(void *data)
{
	Info *info = (Info*)data;

	while (1) {
		printf("[%s] %s\n", info->process, info->name);
		delay(info->delay, 0);
	}
}

void process1()
{
	pthread_t th1, th2;
	Info th1_info = {
		"Process1",
		"Thread1",
		1
	},
	th2_info = {
		"Process1",
		"Thread2",
		1
	};

	printf("[ Process1 ]\n");

	th1 = create_thread(thread, (void*)&th1_info);
	th2 = create_thread(thread, (void*)&th2_info);

	pthread_join(th1, NULL);
	pthread_join(th2, NULL);
}

void process2()
{
	pthread_t th1, th2;
	Info th1_info = {
		"Process2",
		"Thread1",
		2
	},
	th2_info = {
		"Process2",
		"Thread2",
		2
	};

	printf("[ Process2 ]\n");

	th1 = create_thread(thread, (void*)&th1_info);
	th2 = create_thread(thread, (void*)&th2_info);

	pthread_join(th1, NULL);
	pthread_join(th2, NULL);
}

int main(void)
{
	pid_t p1, p2;

	p1 = create_process(&process1);
	p2 = create_process(&process2);

	printf("Create Processes: %d %d\n", p1, p2);

	while(1) {
		delay(1, 0);
	}

	return 0;
}