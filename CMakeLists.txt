cmake_minimum_required(VERSION 3.5)
project(process_thread_test)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -lpthread")

set(SOURCE_FILES main.c)
add_executable(process_thread_test ${SOURCE_FILES})